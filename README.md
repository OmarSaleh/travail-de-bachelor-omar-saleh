# Uniancer

Uniancer est un projet full-stack MERN qui a pour but de gérer les factures et poursuivre le paiement des clients pour les freelancers. 

## Les fichiers

Le projet contient 2 fichiers: Client et Backend

## Prérequis

[Node](https://nodejs.org/en/download/)

[MongoDB](https://www.mongodb.com/atlas/database?utm_campaign=atlas_bc_mern&utm_source=medium&utm_medium=inf&utm_term=campaign_term&utm_content=campaign_content)


## Lancer le projet en local

Ouvrir le fichier Client


```

# installer npm
npm install

# Si il manque la librairie progress/kendo:
npm install @progress/kendo-drawing@^1.2.0 @progress/kendo-licensing@^1.0.1

# Lancer 
npm start

```
Ouvrir le fichier Backend



```

# installer npm
npm install

# Lancer 
npm run server

```

## Docs

[React](https://fr.reactjs.org/)

[MongoDB](https://www.mongodb.com/atlas/database?utm_campaign=atlas_bc_mern&utm_source=medium&utm_medium=inf&utm_term=campaign_term&utm_content=campaign_content)

[Node](https://nodejs.org/en/download/)

[Express](https://expressjs.com/fr/)